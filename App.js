/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createStackNavigator} from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import MainScreen from './Components/MainScreen'; 

const RootStack = createStackNavigator({
  Main: {
    screen: MainScreen,
  },
});

const AppContainer = createAppContainer(RootStack);

export default AppContainer;
