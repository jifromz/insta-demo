import React from 'react';
import {StyleSheet, Image, Text, View, TextInput} from 'react-native';
import {
  Card,
  CardItem,
  Thumbnail,
  Body,
  Left,
  Item,
  Button,
  Icon,
  Input,
} from 'native-base';
import axios from 'axios';

export default class CardComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      post: [],
      disableTextArea: true,
    };
  }

  

  render() {
    const images = {
      '1': require('../assets/batman.jpg'),
      '2': require('../assets/moana.jpg'),
      '3': require('../assets/avenger.jpg'),
    };   
    

    return (
      <Card>
        <CardItem>
          <Left>
            <Thumbnail source={require('../assets/me.jpg')} />
            <Body>
              <Text>Jifford</Text>
              <Text note>Jan 15, 2018</Text>
            </Body>
          </Left>
        </CardItem>
        <CardItem cardBody>
          <Image
            source={images[this.props.imageSource]}
            style={{height: 200, width: null, flex: 1}}
          />
        </CardItem>
        <CardItem style={{height: 45}}>
          <Left>
            <Button transparent>
              <Icon name="md-heart-empty" style={{color: 'black'}} />
            </Button>
            <Button transparent>
              <Icon name="md-chatbubbles" style={{color: 'black'}} />
            </Button>
            <Button transparent>
              <Icon name="ios-send" style={{color: 'black'}} />
            </Button>
          </Left>
        </CardItem>
        <CardItem style={{height: 20}}>
          <Text>{this.props.likes}</Text>
        </CardItem>
        <CardItem>
          <Body>
            <Text>
              <Text style={{fontWeight: 'bold'}}>Jifford </Text>
              <Text>{this.props.post.post}</Text>{' '}
            </Text>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between', alignContent:'flex-end', alignItems:'flex-end'}}>
              <Button transparent onPress={() => {}}>
                <Icon name="ios-eye" style={{color: 'black'}} />
              </Button>
              <Button transparent onPress={() => {this.props.delete(this.props.post.id)}}>
                <Icon name="ios-trash" style={{color: 'black'}} />
              </Button>
            </View>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});


