import React from 'react';
import {StyleSheet, View, Text, ScrollView} from 'react-native';
import {
  Icon,
  Container,
  Content,
  Thumbnail,
  Body,
  Right,
  Left,
  Header
} from 'native-base';
import CardComponent from '../CardComponent';
import axios from 'axios';

class HomeTab extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      posts: []
    }
    this.onDelete = this.onDelete.bind(this);
  }

  componentDidMount(){
    axios.get("http://10.0.2.2:8000/api/post").then(response => {
      this.setState({
        posts: response.data
      }, () => {
        console.log(this.state.posts);
      });
  });
  }

  static navigationOptions = {
    tabBarIcon: ({tintColor}) => (
      <Icon name="md-home" style={{color: tintColor}} />
    ),
  };

  onDelete(post_id) {
    console.log("here" +post_id )
    axios
        .delete("http://10.0.2.2:8000/api/post/delete/" + post_id)
        .then(response => {
            var posts = this.state.posts;

            for (let index = 0; index < posts.length; index++) {
                if (posts[index].id == post_id) {
                  posts.splice(index, 1);
                    this.setState({
                      posts: posts
                    });
                }
            }
            
        })
        .catch(error => {
           
        });
}

 
  render() {
    return (
      <Container style={styles.container}>
        <Header style={{backgroundColor: 'white'}}>
          <Left>
            <Icon name="ios-camera" style={{paddingLeft: 10}}></Icon>
          </Left>
          <Body>
            <Text>Instagram</Text>
          </Body>
          <Right>
            <Icon name="ios-send" style={{paddingLeft: 10}} />
          </Right>
        </Header>
        <Content>
          <View style={{height: 100}}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingHorizontal: 7,
              }}>
              <Text Text style={{fontWeight: 'bold'}}>
                Stories
              </Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Icon name="md-play" style={{fontSize: 14}} />
                <Text style={{fontWeight: 'bold'}}>Watch All</Text>
              </View>
            </View>
            <View style={{flex: 3}}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{
                  alignItems: 'center',
                  paddingStart: 5,
                  paddingEnd: 5,
                }}>
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/1.jpg')}
                />
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/2.jpg')}
                />
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/3.jpg')}
                />
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/4.jpg')}
                />
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/5.jpg')}
                />
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/6.jpg')}
                />
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/7.jpg')}
                />
                <Thumbnail
                  style={{
                    borderWidth: 2,
                    borderColor: 'pink',
                    marginHorizontal: 5,
                  }}
                  source={require('../../assets/StoriesHeaderThumbnails/8.jpg')}
                />
              </ScrollView>
            </View>
          </View>
          {
            this.state.posts.map(post => {
              console.log(post)
              return(
                <CardComponent key={post.id} imageSource="1" likes="101"  post={post} delete={this.onDelete} />
              )
            })
          }
          {/* <CardComponent imageSource="1" likes="101" />
          <CardComponent imageSource="2" likes="201" />
          <CardComponent imageSource="3" likes="301" /> */}
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default HomeTab;
