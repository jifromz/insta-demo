import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {
  Icon,
  Container,
  Content,
  Thumbnail,
  Body,
  Right,
  Left,
  Header,
  Form,
  Textarea,
  Button,
} from 'native-base';
import axios from 'axios';

class PostTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      post: '',
    };
    this.onchangePost = this.onchangePost.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit() {
    

    if (this.state.post == null || this.state.post == '') {
      alert('Please add post.');
    }

    const post = {
      post: this.state.post,
    };

    console.log("helooo")

    axios
      .post('http://10.0.2.2:8000/api/post/store', post)
      .then(res => {
        alert('Post successfully added.');
      })
      .catch(error => {
        alert("Error");
      });
  }

  onchangePost(e) {
    console.log(e);
    this.setState({
        post: e.target.value
    });
}

  static navigationOptions = {
    tabBarIcon: ({tintColor}) => (
      <Icon name="md-search" style={{color: tintColor}} />
    ),
  };
  render() {
    return (
      <View style={styles.container}>
        <Header style={{backgroundColor: 'white'}}>
          <Left>
            <Icon name="ios-camera" style={{paddingLeft: 10}}></Icon>
          </Left>
          <Body>
            <Text>Instagram</Text>
          </Body>
          <Right>
            <Icon name="ios-send" style={{paddingLeft: 10}} />
          </Right>
        </Header>
        <Content>
          <Form>
            <Textarea
              rowSpan={15}
              bordered
              placeholder="Post"
              style={{width: '95%', alignSelf: 'center'}}
              value={this.state.post}
              onChange={this.onchangePost}
            />
          </Form>
          <View
            style={{
              justifyContent: 'space-around',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <Button onPress={() => {
              this.onSubmit()
            }}
              full
              style={{marginTop: 10, width: '90%', alignItems: 'center'}}>
              <Text style={{color: 'white', textAlign: 'center'}}>Save</Text>
            </Button>
          </View>
        </Content>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

export default PostTab;
