import React from 'react';
import {Platform} from 'react-native';
import {Icon} from 'native-base';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createAppContainer} from 'react-navigation';
import HomeTab from './AppTabNavigator/HomeTab';
import PostTab from './AppTabNavigator/PostTab';
import AddMediaTab from './AppTabNavigator/AddMediaTab';
import LikesTab from './AppTabNavigator/LikesTab';
import ProfileTab from './AppTabNavigator/ProfileTab';
 

class MainScreen extends React.Component {
  static navigationOptions = {
   header: null
  };
  render() {
    return <AppContainer />;
  }
}

const AppTabNavigator = createBottomTabNavigator(
  {   
    HomeTab: {
      screen: HomeTab,
    },
    PostTab: {
      screen: PostTab,
    },
    AddMediaTab: {
      screen: AddMediaTab,
    },
    LikesTab: {
      screen: LikesTab,
    },
    ProfileTab: {
      screen: ProfileTab,
    },
  },
  {
    animationEnabled: true,
    swipeEnabled: true,
    tabBarPosition: 'bottom',
    tabBarOptions: {
      style: {
        ...Platform.select({
          android: {
            backgroundColor: 'white',
          },
        }),
      },
      activeTintColor: '#000',
      inactiveTintColor: '#E0E0E0',
      showLabel: false,
      showIcon: true,
    },
  },
);

const AppContainer = createAppContainer(AppTabNavigator);

export default MainScreen;
